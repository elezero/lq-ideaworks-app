package com.elesteam.lqideaworksapp.PokemonAPI;

import com.elesteam.lqideaworksapp.Modelos.PokemonRespuesta;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PokemonApiService {

    @GET("pokemon")
    Call<PokemonRespuesta> obtenerListaPokemones(@Query("limit") int limite);

}
