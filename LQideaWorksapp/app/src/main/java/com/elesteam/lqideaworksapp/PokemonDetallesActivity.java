package com.elesteam.lqideaworksapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class PokemonDetallesActivity extends AppCompatActivity {
    Button btn_regresar;
    ToggleButton tbtn_favorito;
    ImageView img_pokemon;
    TextView txt_altura;
    TextView txt_experiencia;
    TextView txt_generacion;
    TextView txt_numero;
    TextView txt_nombre;
    TextView txt_estadisticas;
    TextView txt_weight;
    TextView txt_habilidades;
    TextView txt_movimientos;
    TextView txt_articulos;

    String numero;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    //LISTENERS PARA TOOGLES BUTTONS
    //tooglebutton favorito
    CompoundButton.OnCheckedChangeListener favorito_btn_listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            editor.putBoolean("favorito_"+numero, b);
            editor.commit();

            if(b)
                Toast.makeText(getApplicationContext(), getString(R.string.favorite_added), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getApplicationContext(), getString(R.string.favorite_removed), Toast.LENGTH_SHORT).show();
        }
    };
    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_detalles);


        sharedPreferences = getSharedPreferences("POKEDEX", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        //Obteniendo numero (pasado como parametro de la actividad anterior)
        numero = getIntent().getStringExtra("number");

        //LINKEANDO ELEMENTOS DEL LAYOUT
        btn_regresar = findViewById(R.id.btn_regresar);
        tbtn_favorito = findViewById(R.id.tbtn_favorito);
        img_pokemon = findViewById(R.id.img_pokemon);
        txt_altura = findViewById(R.id.txt_altura);
        txt_experiencia = findViewById(R.id.txt_experiencia);
        txt_generacion = findViewById(R.id.txt_generacion);
        txt_numero = findViewById(R.id.txt_numero);
        txt_nombre = findViewById(R.id.txt_nombre);
        txt_estadisticas = findViewById(R.id.txt_estadisticas);
        txt_weight = findViewById(R.id.txt_weight);
        txt_habilidades = findViewById(R.id.txt_habilidades);
        txt_movimientos = findViewById(R.id.txt_movimientos);
        txt_articulos = findViewById(R.id.txt_articulos);


        //REQUEST QUEUE
        mQueue = Volley.newRequestQueue(getApplicationContext());


        //Obteniendo datos del pokemon con JSON
        obtenerDatosPokemon(getApplicationContext());


        //Si lo tiene agregado como favorito, marcar como chekeado el boton de favorito
        if(sharedPreferences.getBoolean("favorito_"+numero, false)){
            tbtn_favorito.setChecked(true);
        }


        //Boton regresar a la actividad anterior
        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        //Boton que detecta cuando cambia el boton de favorito
        tbtn_favorito.setOnCheckedChangeListener(favorito_btn_listener);
    }

    //TODO: Obtiene los datos del pokemon que se van a mostrar en pantalla, lo hace mediante
    // JsonObjectRequest para no utilizar Retrofit y tener que hacer todas los modelos, ya que
    // con este no es necesario utilizar modelos, puedo obtener atributos especificos
    private void obtenerDatosPokemon(Context context){

        //OBTENIENDO IMAGEN
        String image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+numero+".png";

        Glide.with(context)
                .load(image_url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(img_pokemon);


        // JSON PARSE
        String url = "https://pokeapi.co/api/v2/pokemon/"+numero;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("MYJSON", "onResponse");

                        try {
                            //TODO: Obteniendo estadisticas
                            JSONArray stats = response.getJSONArray("stats");
                            String string_estadisticas = getString(R.string.stats);

                            for(int i=0;i<stats.length();i++){
                                JSONObject stat = stats.getJSONObject(i);
                                JSONObject stat_stat = stat.getJSONObject("stat");


                                string_estadisticas += stat_stat.getString("name") + ":"+stat.getString("base_stat") + " ";
                            }

                            //TODO: Obteniendo abilidades
                            JSONArray abilities = response.getJSONArray("abilities");
                            String string_habilidades = "";

                            for(int i=0;i<abilities.length();i++){
                                JSONObject ability = abilities.getJSONObject(i);
                                JSONObject ability_ability = ability.getJSONObject("ability");


                                string_habilidades += ability_ability.getString("name") + " ";
                            }

                            //TODO: Obteniendo movimientos
                            JSONArray moves = response.getJSONArray("moves");
                            String string_movimientos = "";

                            for(int i=0;i<moves.length();i++){
                                JSONObject move = moves.getJSONObject(i);
                                JSONObject move_move = move.getJSONObject("move");


                                //SOLO VOY A MOSTRAR LOS PRIMEROS 3 MOVIMIENTOS PARA NO
                                // OCUPAR MUCHO ESPACIO EN LA PANTALLA
                                if(i<3){
                                    string_movimientos += move_move.getString("name") + " ";
                                }

                            }


                            //Agregando la informacion a la vista
                            txt_weight.setText(getString(R.string.weight) + response.getString("weight"));
                            txt_altura.setText(getString(R.string.height) + response.getString("height"));
                            txt_experiencia.setText(getString(R.string.base_experience) + response.getString("base_experience"));
                            txt_nombre.setText(getString(R.string.name) + response.getString("name"));
                            txt_estadisticas.setText(string_estadisticas);
                            txt_habilidades.setText(string_habilidades);
                            txt_movimientos.setText(string_movimientos);
                            txt_numero.setText(getString(R.string.id_number) + numero);


                        } catch (JSONException e) {
                            e.printStackTrace();

                            Toast.makeText(getApplicationContext(), getString(R.string.error_get_pokemon_data), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("MYJSON", "onErrorResponse");
                Toast.makeText(getApplicationContext(), getString(R.string.error_get_pokemon_data), Toast.LENGTH_SHORT).show();
            }

        });
        mQueue.add(request);
    }
}