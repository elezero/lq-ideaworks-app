package com.elesteam.lqideaworksapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.elesteam.lqideaworksapp.Adaptadores.ListaPokemonAdapter;
import com.elesteam.lqideaworksapp.Modelos.Pokemon;
import com.elesteam.lqideaworksapp.Modelos.PokemonRespuesta;
import com.elesteam.lqideaworksapp.PokemonAPI.PokemonApiService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FavoritosActivity extends AppCompatActivity {

    Retrofit retrofit;
    SharedPreferences sharedPreferences;
    private RecyclerView recyclerView;
    private ListaPokemonAdapter listaPokemonAdapter;
    private Button btn_regresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoritos);

        setTitle(getString(R.string.favorites));

        //Grids para la lista de pokemones
        recyclerView = findViewById(R.id.recyclerView);
        listaPokemonAdapter = new ListaPokemonAdapter(this);
        recyclerView.setAdapter(listaPokemonAdapter);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        //Boton para regresar
        btn_regresar = findViewById(R.id.btn_regresar);

        sharedPreferences = getSharedPreferences("POKEDEX", Context.MODE_PRIVATE);

        retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Obtiene la lista de pokemones con la libreria Retrofit
        obtenerPokemons();

        //Asignacion de funcion al presionar boton de atras (no fisico)
        btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    //TODO: Funcion para obtener los pokemones de la API de Pokemon con Retrofit
    private void obtenerPokemons(){
        PokemonApiService service = retrofit.create(PokemonApiService.class);
        Call<PokemonRespuesta> pokemonRespuestaCall = service.obtenerListaPokemones(20);

        pokemonRespuestaCall.enqueue(new Callback<PokemonRespuesta>() {
            @Override
            public void onResponse(Call<PokemonRespuesta> call, Response<PokemonRespuesta> response) {
                if(response.isSuccessful()){
                    PokemonRespuesta pokemonRespuesta = response.body();

                    ArrayList<Pokemon> listaPokemon = pokemonRespuesta.getResults();
                    ArrayList<Pokemon> listaPokemonFavoritos = new ArrayList<>();

                    for(int i = 0; i< listaPokemon.size(); i++){
                        Pokemon pokemon = listaPokemon.get(i);

                        //numero
                        String[] urlPartes = pokemon.getUrl().split("/");
                        int numero = Integer.parseInt(urlPartes[urlPartes.length - 1]);

                        pokemon.setNumber(numero);


                        //Si esta en favoritos agregarlo al arraylist
                        if(sharedPreferences.getBoolean("favorito_"+numero, false)){
                            listaPokemonFavoritos.add(pokemon);
                        }
                    }

                    listaPokemonAdapter.mostrarListaPokemon(listaPokemonFavoritos);

                }else{
                    Toast.makeText(getApplicationContext(), getString(R.string.error_get_data_pokemones), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PokemonRespuesta> call, Throwable t) {
                Toast.makeText(getApplicationContext(), getString(R.string.error_get_data_pokemones), Toast.LENGTH_SHORT).show();
            }
        });
    }
}