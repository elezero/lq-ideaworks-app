package com.elesteam.lqideaworksapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.elesteam.lqideaworksapp.Adaptadores.ListaPokemonAdapter;
import com.elesteam.lqideaworksapp.Modelos.Pokemon;
import com.elesteam.lqideaworksapp.Modelos.PokemonRespuesta;
import com.elesteam.lqideaworksapp.PokemonAPI.PokemonApiService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    Retrofit retrofit;
    private RecyclerView recyclerView;
    private ListaPokemonAdapter listaPokemonAdapter;
    private Button btn_favoritos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = findViewById(R.id.recyclerView);
        listaPokemonAdapter = new ListaPokemonAdapter(this);
        recyclerView.setAdapter(listaPokemonAdapter);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        //Boton de favoritos (para ir a la actividad de favoritos)
        btn_favoritos = findViewById(R.id.btn_regresar);


        retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        //Obtiene la lista de pokemones con Retrofit de la API de Pokemon
        obtenerPokemons();


        btn_favoritos.setOnClickListener(btn_favoritos_listener);
    }


    //LISTENER DEL CLICK EN EL BOTON FAVORITOS PARA IR A LA ACTIVIDAD DE FAVORITOS
    View.OnClickListener btn_favoritos_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent gtFavoritos = new Intent(getApplicationContext(), FavoritosActivity.class);
            startActivity(gtFavoritos);
        }
    };

    private void obtenerPokemons(){
        PokemonApiService service = retrofit.create(PokemonApiService.class);
        Call<PokemonRespuesta> pokemonRespuestaCall = service.obtenerListaPokemones(20);

        pokemonRespuestaCall.enqueue(new Callback<PokemonRespuesta>() {
            @Override
            public void onResponse(Call<PokemonRespuesta> call, Response<PokemonRespuesta> response) {
                if(response.isSuccessful()){
                    PokemonRespuesta pokemonRespuesta = response.body();

                    ArrayList<Pokemon> listaPokemon = pokemonRespuesta.getResults();

                    for(int i = 0; i< listaPokemon.size(); i++){
                        Pokemon pokemon = listaPokemon.get(i);

                        //numero
                        String[] urlPartes = pokemon.getUrl().split("/");
                        int numero = Integer.parseInt(urlPartes[urlPartes.length - 1]);

                        pokemon.setNumber(numero);
                    }
                    listaPokemonAdapter.mostrarListaPokemon(listaPokemon);

                }else{
                    Log.d("MYPOKEMON", String.valueOf(response.errorBody()));
                    Toast.makeText(getApplicationContext(), getString(R.string.error_get_data_pokemones), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PokemonRespuesta> call, Throwable t) {
                Log.d("MYPOKEMON", t.toString());
                Toast.makeText(getApplicationContext(), getString(R.string.error_get_data_pokemones), Toast.LENGTH_SHORT).show();
            }
        });
    }
}