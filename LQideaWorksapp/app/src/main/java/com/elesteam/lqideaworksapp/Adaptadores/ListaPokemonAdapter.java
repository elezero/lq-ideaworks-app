package com.elesteam.lqideaworksapp.Adaptadores;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elesteam.lqideaworksapp.PokemonDetallesActivity;
import com.elesteam.lqideaworksapp.R;
import com.elesteam.lqideaworksapp.Modelos.Pokemon;

import java.util.ArrayList;

public class ListaPokemonAdapter extends RecyclerView.Adapter<ListaPokemonAdapter.ViewHolder> {

    private ArrayList<Pokemon> dataset;
    private Context context;


    public ListaPokemonAdapter(Context context){
        this.context = context;
        dataset = new ArrayList<>();
    }

    @NonNull
    @Override
    public ListaPokemonAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ListaPokemonAdapter.ViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ir al detalle
                Intent gtDetalles = new Intent(context, PokemonDetallesActivity.class);
                gtDetalles.putExtra("number", String.valueOf(dataset.get(position).getNumber()));
                context.startActivity(gtDetalles);
            }
        });

        Pokemon pokemon = dataset.get(position);
        holder.txt_nombre.setText(pokemon.getName());
        holder.txt_numero.setText("#"+pokemon.getNumber());

        String image_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+pokemon.getNumber()+".png";

        Glide.with(context)
                .load(image_url)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.img_pokemon);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }



    public void mostrarListaPokemon(ArrayList<Pokemon> listaPokemon){
        dataset.addAll(listaPokemon);
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_pokemon;
        private TextView txt_numero;
        private TextView txt_nombre;

        public ViewHolder(View itemView){
            super(itemView);

            img_pokemon = itemView.findViewById(R.id.img_pokemon);
            txt_numero = itemView.findViewById(R.id.txt_numero);
            txt_nombre = itemView.findViewById(R.id.txt_nombre);
        }
    }
}
