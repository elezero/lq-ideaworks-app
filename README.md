

# LQ IdeaWorks App

App para el proceso de seleccion en IdeaWorks
por Luis Quijada <jose.jlq@hotmail.com>


Para probar la aplicacion, busque el APK en la carpeta principal de este repositorio llamado app-release.apk.


.
**Version finalizada:**

 - Lista de pokemons completa, filtrado y limitado a 20 pokemones, enviando este numero como parametro dependiendo de cuantos se quieren filtrar
 - Icono de la aplicacion diseñado de acuerdo a los colores recomendados en el wireframe, y agregado a la aplicación asi como tambien al splashscreen
 - SplashScreen personalizado como pantalla de inicio y con el icono de la aplicación y texto personalizado, la cual cambia automaticamente a la actividad Main
 - Imagenes agregadas
 - Estilo idéntico al solicidato (Wireframe)
 
 **Bonus Agregados**
 
 - Aplicación traducida al español e ingles automaticamente dependiendo del idioma del dispositivo.
 - Permite que el usuario guarde los pokemons como favoritos y en una pantalla aparte se ven unicamente los que tiene como favoritos.
 - Icono de la aplicación personalizado, siguiendo los colores del wireframe.
 - Manejo de errores en la conectividad con mensajes de errores personalizados y traducidos al idioma como se menciona en el punto 1.
 - Animación de inicio (splashscreen) agregada.
 
 
 **Librerias utilizadas**
 
 - Glide: Para el manejo de las imagenes (https://github.com/bumptech/glide)
 - Retrofit: Para consumir servicios RESTs, por lo tanto la API de Pokemon (https://github.com/square/retrofit)
 - Volley: Libreria HTTP
 

**Comentarios**
*Para consumir la API de Pokemon utilice 2 formas, primero lo hice mediante Retrofit con el cual obtengo la lista de pokemones (hasta 20), y luego utilizando Volley obtuve los demas detalles y atributos de cada uno de los pokemons, esto debido a que en las instrucciones me limitaban a no usar generadores de codigo y la ventaje que con Volley no tuve que crear todo el modelo completo.*